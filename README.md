# CS348K Final Project

## Proposal Document 

* __Project Title:__  Architectural Improvement of Hardware Tailored for SLAM Algorithms in Real-time AR/VR Applications

* __Names and SuNET ID's__ Pu Deng (piaodeng), Binxu Li (andy0207), Lixian Yan (lxyn5869)

* __Summary:__ We are going to evalutate the feasibility and performance improvement of mapping SLAM algorithoms to a 8-chiplet illusion system with no off-chip memory. The core idea of the illusion system is to avoid off-chip memory access by spreading the computation to multiple chips and use only on-chip buffers of these chips. We will first analyze current popular and state-of-the-art SLAM algorithms and choose one SLAM algorithm that has sufficient accuracy while fitting into our chiplet system, particularly the memory constrain. Then, for the chosen algrotithm, we will analyze its computation pattern and characteristics, attempting to simplify the complex algorithms to basic compute blocks and plot its dataflow graph. Next, we will use ZigZag, a DNN accelerator design space exploration tool, to search an optimized memory hierarchy and CNN layers blocking scheme. By the end of the project, we will compare the SLAM algorithms' costs and present the simulated improvements compared with GPU implementation.

* __Inputs and outputs:__ List the inputs and outputs of the implementation, as well as the major constraints on your design (is the fundamental constraint machine performance? is it human time?).  In this section I want you to take lessons from the papers we've read that did a good job of articulating this problem setup. 
  - Inputs: Video stream from EuRoc dataset.
  - System Constraints: For SLAM algorithm: no lidar sensors due to power limitation, no RGB-D camera for outdoor functionality. For accelerator: no off-chip DRAM.
  - Algorithm Constraints: robustness, need to avoid tracking failure in most environments. Need to have low memory consumption for no DRAM implementation. Need to have reasonable computation to achieve 30fps performance.
  - Outputs: Camera poses.

* __Task list:__ No more than a few paragraphs of description of what you will do.  If your project is about algorithmic innovation, what is the basic proposed approach?  If your project is about implementing an existing paper, list the parts of the paper you will need to implement.  Specifically, please make sure this section has:
    
    * A short list of things you will implement in order to "complete the project" (i.e., you expect to get a desirable grade if you execute on all of these)  __A VERY, VERY STRONG SUGGESTION is to make your first couple of tasks whatever needs to be done to get a code base running end-to-end.__  That is:
        * Download, compile, and run starter code on a simple "hello world" example or dataset. Applicable, please describe where you are getting the started code, start datasets, etc. from.
        * Pick (or create) the dataset that you will use on a daily basis throughout the project timeline.
        * Implement the baseline algorithm that your more advanced algorithm will be compared against.
        * Get the application to generate correct results (even without any optimization), etc. 
        * In other words, I want your projects to always be in a state where you can stop and evaluate how well you are doing end-to-end. Therefore, additional work always simply improves on the current results. __Your goal in the first week of the project should be *AT LEAST* to get to the state where all baselines are in place, or your code is running end-to-end on a trivial example__

    __Done__
    - [x] Review some popular visual SLAM algorithms, choosed DPVO as the algorithm to implement.
    - [x] Ran the DPVO on GPU server and benchmarked the computation requirements as well as the memory consumption.
    - [x] Analyzed DPVO algorithm and summarized it in a flow diagram. See DPVOarch.pdf. Based on computation and data traffic pattern of the workload, we estimate that we are going to have a 64x64 systolic array as well as a 128 bit main NoC to achieve 30 frame per second.
    - [x] Find ways to partition data storage in different chiplets. Currently implemented sequential and round robin strategies. Test the results on EuRoC MH01 data set.


    __To-do__
    - [ ] Optimize the frame partition scheme to different chips to reduce inter chip message.
    - [ ] Use Zigzag to find a memory hierarchy for the accelerator.
    - [ ] Do quantization to the inputs and weights to 8bit integer to reduce power.
  
  * Nice to have if ahead of schedule: 
     * modify the original DPVO code to emulate the behavior of the chiplets and data storage partitions.

  * Student Responsibilities: 
     * Pu Deng: performed the study on the SLAM algorithms, use zigzag to do systolic array design space exploration. do the data storage partition scheme exploration.
     * Binxu Li: also worked on understanding the SLAM algorithms, work on quantization of the DPVO.
     * Lixian Yan: Find the MRAM macros specs in the literature and code memmory wrappers in the DES tool. Collaborate on CNN acceleration design and performance analysis.

* __Expected deliverables and/or evaluation.__ This is where I want you to focus on what demo you are going to show during your presentation, or what [sequence of] graphs you hope to make in your report.  This is the place where I'd like to see the most detail in your proposal, since if you define a clear goal, your project activities will involve just working back from this goal to determine what needs to be done.  Are you trying to demonstrate an application, scheduled via Halide running at 30 fps on your laptop?  Are you going to demonstrate reasonable accuracy models that were trained in 30 minutes of labeling work? Are you going to demonstrate a CUDA ray tracer that uses a BVH build using techniques from advanced graphics papers? Is there a particular image you want to create? __Specifically, I want you to consider and write down how will you evaluate/determine the extent to which you were successful.__

Ultimatly, we need to demonstrate a chip architecture and a scheduling of the DPVO to achieve 30fps and smaller or equal to 10w of power consumption in 28nm node. We will use the Zigzag tool to estimate the latency and energy consumption for execution of each layer, use a python model to emulate the computation and message traffic between chips to estimate the total latency as well as the power consumption of the system.

* __What are the biggest risks?__ Please document the biggest risks in the project.  Often there are points or blockers such as, if I can't get this code to compile/run, then I can't do the work.  Or, until I am successfully training this DNN and have reasonable trained model, I can't do aynthing else.  I want you to think through the risks on your project, and consider how to eliminate/derisk these aspects of the project with as little work as possible.

Since we do not have the direct help from the original author, we may misunderstood the algorithm and model it incorrectly. It is also impossible for us to reimplement the entire program due to time limitation. We will try to split the original DPVO algorithm into multiple processes and use the processes to isolate their computation and data and emulate the behavior of each chiplets.

* __What you need help with?__. What advice would like from Kayvon? Are there papers you need references to? Do you need a machine or computing resources to succeed?

## Final Report

[Link to the overleaf final report draft](https://www.overleaf.com/read/gbnppwjqnbbz#0ca89f)

